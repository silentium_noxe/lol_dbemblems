<?php

include_once "FileLogger.php";

class CustomFileLogger implements FileLogger
{
    public function log($fileName, $data)
    {
        file_put_contents($fileName, $data, FILE_APPEND);
    }
}