var previewImg = {
    statusLoading: false
};

function toClickBoard(img_emblem) {
    if (! navigator.clipboard){
        return;
    }
    navigator.clipboard.writeText(img_emblem.alt).then(
        function (value) { alert("unique string is copied")},
        function (err) { alert("unique string not copied")});
}

function loadImg(input) {
    previewImg.statusLoading = false;
    if(getLongURLImage(input.value) === ""){
        document.getElementById("img_loaded").style.display = "none";
        return;
    }

    document.getElementById("img_loaded").setAttribute("src", getLongURLImage(input.value));
    document.getElementById("img_loaded").style.display = "block";
}

function getLongURLImage(url) {
    if(url === "" || url === " "){
        return "";
    }

    if((url.indexOf("https://www.landsoflords.com")) >= 0){
        return url;
    }else {
        return "https://www.landsoflords.com" + url;

    }
}

function sendNewEmblem(form) {
    if (!previewImg.statusLoading){
        alert("Wrong URL of image");
        return;
    }
    var urlImg;
    urlImg = form["url"].value;
    var pattern = "https://www.landsoflords.com";
    if(urlImg.indexOf(pattern) >= 0){
        urlImg = urlImg.substr(pattern.length);
    }
    pattern = ".st.jpg";
    if(urlImg.indexOf(pattern) < 0){
        urlImg = urlImg.substr(0, urlImg.length - pattern.length) + pattern;
    }
    var tags;
    if(form["tags"].value === null || form["tags"].value === ""){
        return false;
    }else{
        tags = form["tags"].value;
    }

    var method = "GET";
    var url = "saveEmblem.php?url="+urlImg+"&tags="+tags;
    var asyn = true;

    var xhr = new XMLHttpRequest();
    xhr.open(method, url, asyn);
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200){
            alert("Save success");
            form.reset();
        }else if (this.readyState === XMLHttpRequest.DONE && this.status !== 200){
            console.log("respText: "+this.responseText);
            alert("Something went wrong, try later");
            form.reset();
        }
    };
    xhr.send();
}

function loadNewEmblems(direction) {
    if(direction>0){
        var currentURL = window.location.href;
        var pos;
        if ((pos = currentURL.indexOf("st=")) > 0){
            console.log(pos);
        }
    }
}

function searchSelectedTag(element){
    if (element.selectedIndex === 0){
        return;
    }
    document.getElementById("searchByTags").value += element[element.selectedIndex].text+" ";
}

function hideChangelog() {
    document.getElementById("mainBlock-changelog").hidden = true;
}

function showChangelog() {
    document.getElementById("mainBlock-changelog").hidden = false;
}

function hideNotLoadedImage(element){
    element.hidden = true;
}