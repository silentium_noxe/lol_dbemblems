function ImageList() {
    this.offset = 0;
    this.showedEmblems = 0;
    this.tags = "";
    this.maxEmblems = document.getElementById("numberEmblems").innerText;

    this.loadNextImages = function () {
        var method = "POST";
        var url = "searchImages.php";
        var asyn = true;

        var xhr = new XMLHttpRequest();
        xhr.open(method, url, asyn);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        var currentContext = this;
        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200){
                document.getElementById("listEmblems").innerHTML = this.responseText;
                currentContext.showedEmblems += document.querySelectorAll(".img-emblem").length;

                if (currentContext.showedEmblems >= currentContext.maxEmblems){
                    currentContext.hideRightButton();
                }
                currentContext.showLeftButton();
            }
        };

        this.offset += 20;
        var data = "offset="+this.offset+"&tags="+this.tags;
        this.showLeftButton();

        xhr.send(data);
    };

    this.loadPreviousImages = function () {
        var method = "POST";
        var url = "searchImages.php";
        var asyn = true;

        var xhr = new XMLHttpRequest();
        xhr.open(method, url, asyn);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        var amountOldImage = document.querySelectorAll(".img-emblem").length;

        var currentContext = this;
        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200){
                document.getElementById("listEmblems").innerHTML = this.responseText;

                currentContext.showedEmblems -= amountOldImage;
                currentContext.showRightButton();
                if(currentContext.showedEmblems <= 20){
                    currentContext.hideLeftButton();
                }
            }
        };

        if (this.offset >= 20) {
            this.offset -= 20;
            var data = "offset="+this.offset+"&tags="+this.tags;
        }else{
            this.hideLeftButton();
            return;
        }

        xhr.send(data);
    };

    this.loadFirstImages = function () {
        if (this.tags === "" || this.tags === " "){
            this.tags = "none";
        }

        var method = "POST";
        var url = "searchImages.php";
        var asyn = true;

        var xhr = new XMLHttpRequest();

        xhr.open(method, url, asyn);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        var currentContext = this;
        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200){
                document.getElementById("listEmblems").innerHTML = this.responseText;
                currentContext.showedEmblems += document.querySelectorAll(".img-emblem").length;
                if (currentContext.showedEmblems < 20){
                    currentContext.hideRightButton();
                } else{
                    currentContext.showRightButton();
                }
            }
        };

        var data = "offset="+this.offset+"&tags="+this.tags;
        xhr.send(data);
    };

    this.loadByTags = function(inputTags){
        if (inputTags.value === this.tags){
            return;
        }
        this.tags = inputTags.value;
        this.offset = 0;
        this.showedEmblems = 0;
        this.loadFirstImages();
    };

    this.pressButtonLoadByTags = function() {
        this.loadByTags(document.getElementById("searchByTags"));
    };

    this.showLeftButton = function () {
        document.getElementById("loadPreviousEmblems").hidden = false;
    };

    this.hideLeftButton = function () {
        document.getElementById("loadPreviousEmblems").hidden = true;
    };

    this.showRightButton = function () {
        document.getElementById("loadNextEmblems").hidden = false;
    };

    this.hideRightButton = function () {
        document.getElementById("loadNextEmblems").hidden = true;
    };

    this.leftButtonIsAvailable = function () {
        return !document.getElementById("loadPreviousEmblems").hidden;
    };

    this.rightButtonIsAvailable = function () {
        return !document.getElementById("loadNextEmblems").hidden;
    }
}
