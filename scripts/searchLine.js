var datalist = document.querySelector('#listTags');
var options = datalist.querySelectorAll('option');

var valueOptionsArray = [];
options.forEach(function (item) {
    valueOptionsArray.push(item.value);
});

var input = document.querySelector('input[list]');
var inputspace = (input.value.match(/ /g)||[]).length;
var separator = ' ';

function filldatalist(prefix) {
    if (input.value.indexOf(separator) > -1 && options.length > 0) {
        datalist.innerHTML = "";
        valueOptionsArray.forEach(function (optionValue) {
            if (prefix.indexOf(optionValue) < 0 ) {

                var newOption = document.createElement("option");
                newOption.value = prefix+optionValue;
                datalist.append(newOption);
            }
        });
    }else if(options.length > 0){
        datalist.innerHTML = "";
        valueOptionsArray.forEach(function (optionValue) {
            if (prefix.indexOf(optionValue) < 0 ) {
                var newOption = document.createElement("option");
                newOption.value = optionValue;
                datalist.append(newOption);
            }
        });
    }
}

function addListenerMulti(element, eventNames, listener) {
    var events = eventNames.split(' ');
    for (var i=0, iLen=events.length; i<iLen; i++) {
        element.addEventListener(events[i], listener, false);
    }
}

addListenerMulti(input, "change paste keyup", function() {
    var inputtrim = input.value.replace(/^\s{2,}|\s{2,}$/, " ");
    var currentspace = (input.value.match(/ /g)||[]).length;

        var lsIndex = inputtrim.lastIndexOf(separator);
        var str = (lsIndex !== -1) ? inputtrim.substr(0, lsIndex)+" " : "";
        filldatalist(str);

        inputspace = currentspace;
        input.value = inputtrim;
});