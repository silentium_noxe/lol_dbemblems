var imageList;

function initPage() {
    imageList = new ImageList();
    imageList.loadFirstImages();

    addListenerKeyboardToPage();
    // document.getElementById("loadNextEmblems").addEventListener("click", imageList.loadImages("next"));
}

function addListenerKeyboardToPage() {
    document.body.addEventListener("keyup", function (event) {
        var keyCode = event.keyCode;
        switch (keyCode){
            case 13: imageList.loadByTags(document.getElementById("searchByTags"));
            break;
            case 37:
                console.log("LEFT BUTTON");
                if (imageList.leftButtonIsAvailable()){
                    document.getElementById("loadPreviousEmblems").click();
                }
                break;
            case 39:
                console.log("RIGHT BUTTON");
                if (imageList.rightButtonIsAvailable()){
                    document.getElementById("loadNextEmblems").click();
                }
                break;
            default: console.log(keyCode);
        }
    });
}