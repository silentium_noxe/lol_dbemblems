<?php

include "Emblem.php";
include "EmblemDAO.php";
include_once "functions.php";
include_once "CustomFileLogger.php";

echo "<title>Emblems of landsoflords</title>";

$logger = new CustomFileLogger();
$url = $_GET["url"];
$strTags = $_GET["tags"];
$strTags = strtolower($strTags);
logClientSaving($logger, $url, $strTags);
if ($strTags == "" || $strTags == " "){
    http_response_code(500);
    return;
}

$arrTags = explode(" ", $strTags);
for($i = 0; $i < count($arrTags); $i++){
    if ($arrTags[$i] == ""){
        array_splice($arrTags, $i);
    }
}

$mysqli_connection = openMySqlConnection("localhost", "root", "root", "sn_LoL");

$emblem = new Emblem($url);
foreach ($arrTags as $tag){
    $emblem->addTag($tag);
}

$emblemDAO = new EmblemDAO();
$emblemDAO->setDbConnection($mysqli_connection);
$status = $emblemDAO->create($emblem);
if ($status) {
    http_response_code(200);
}else{
    http_response_code(500);
}

closeMySqlConnection($mysqli_connection);
