<?php

include_once "EmblemContainer.php";

//TODO: change all mentions URL and LINK to uniqString
class EmblemDAO{
    /**
     * @var \mysqli
     */
    private $dbConnection;
    const TABLE_NAME = "copy_Emblems_LoL";

    public function create(Emblem $emblem): bool{
        if (($found = $this->readByUniqueString($emblem->getUniqueString())) != null){
            foreach ($emblem as $tag) {
                if (!$found->containTag($tag)){
                    $this->addTag($emblem->getUniqueString(), $tag);
                }
            }
            return true;
        }

        if (mysqli_begin_transaction($this->dbConnection)){

            foreach ($emblem->getTags() as $tag){
                $query = "INSERT INTO ".EmblemDAO::TABLE_NAME."(uniqString, tag) 
                            VALUES('".$emblem->getUniqueString()."', '".$tag."');";
                mysqli_query($this->dbConnection, $query);
            }
            if(!mysqli_commit($this->dbConnection)){
                return false;
            }
        }else{
            return false;
        }
        return true;
    }

    public function read(string $query, EmblemContainer $container=null): EmblemContainer{
        $result = mysqli_query($this->dbConnection, $query);
        if ($container == null) {
            $container = new EmblemContainer();
        }
        for ($i = 0; $i < $result->num_rows; $i++){
            $arr_row = $result->fetch_row();
//            var_dump($arr_row);
            $emblem = new Emblem($arr_row[0]);
            $container->addEmblem($emblem);
        }
        return $container;
    }

    public function readDiapasonEmblemsWithoutTag(int $amount, int $offset, EmblemContainer $container = null){
        if ($container == null){
            $container = new EmblemContainer();
        }

        $arrayUniqueStrings = $this->readDiapasonUniqueStrings($amount, $offset);
        foreach ($arrayUniqueStrings as $uniqueString){
            $tagsEmblemByUniqueString = $this->readTagsEmblemByUniqueString($uniqueString);
            if (count($tagsEmblemByUniqueString) == 0){
                continue;
            }

            $emblem = new Emblem($uniqueString);
            foreach ($tagsEmblemByUniqueString as $tag){
                $emblem->addTag($tag);
            }
            $container->addEmblem($emblem);
        }

        return $container;
    }

    public function readDiapasonEmblemsWithTags(int $amount, int $offset, array $tags, EmblemContainer $container=null){
        if ($container == null){
            $container = new EmblemContainer();
        }

        $arrayUniqueStrings = $this->readDiapasonUniqueStringsByTags($amount, $offset, $tags);
        foreach ($arrayUniqueStrings as $uniqueString){
            $tagsEmblemByUniqueString = $this->readTagsEmblemByUniqueString($uniqueString);
            if (count($tagsEmblemByUniqueString) == 0){
                continue;
            }

            $emblem = new Emblem($uniqueString);
            foreach ($tagsEmblemByUniqueString as $tag){
                $emblem->addTag($tag);
            }
            $container->addEmblem($emblem);
        }

        return $container;
    }

    public function readAllTags(): array {
        $query = "SELECT tag FROM ".self::TABLE_NAME." GROUP BY tag ";
        $result = mysqli_query($this->dbConnection, $query);
        $tags[] = array();
        for ($i = 0; $i < $result->num_rows; $i++){
            $arr_row = $result->fetch_row();
            $tags[] = $arr_row[0];
        }
        return $tags;
    }

    public function getAmountEmblems(){
        $query = "SELECT COUNT(DISTINCT uniqString) FROM ".self::TABLE_NAME;
        $res = mysqli_query($this->dbConnection, $query);
        return $res->fetch_row()[0];
    }

    public function readByUniqueString(string $uniqueString){
        $tagsEmblemByUniqueString = $this->readTagsEmblemByUniqueString($uniqueString);
        if (count($tagsEmblemByUniqueString) == 0){
            return null;
        }

        $emblem = new Emblem($uniqueString);
        foreach ($tagsEmblemByUniqueString as $tag){
            $emblem->addTag($tag);
        }

        return $emblem;
    }

    public function readAllByTag(string $tag, EmblemContainer $container=null): EmblemContainer{
        $query = "SELECT * FROM ".EmblemDAO::TABLE_NAME." WHERE tag LIKE '$tag'";
        $result = mysqli_query($this->dbConnection, $query);
        if ($container == null) {
            $container = new EmblemContainer();
        }
        for ($i = 0; $i < $result->num_rows; $i++){
            $arr_row = $result->fetch_row();
            $emblem = new Emblem($arr_row[1]);
            $emblem->addTag($arr_row[2]);
            $container->addEmblem($emblem);
        }
        return $container;
    }

    public function addTag(string $url, string $tag){
        $query = "INSERT INTO ".EmblemDAO::TABLE_NAME."(`uniqString`, `tag`) VALUES($url, $tag)";
        mysqli_begin_transaction($this->dbConnection);
        mysqli_query($this->dbConnection, $query);
        return mysqli_commit($this->dbConnection);
    }

    private function readDiapasonUniqueStrings(int $amount, int $offset): array {
        $query = "SELECT `uniqString` FROM ".self::TABLE_NAME." GROUP BY `uniqString` LIMIT $amount OFFSET $offset";
        $resultQuery = mysqli_query($this->dbConnection, $query);

        $arrayUniqueStringOfEmblems = array();
        for ($i = 0; $i < $resultQuery->num_rows; $i++){
            $arr_row = $resultQuery->fetch_row();
            $arrayUniqueStringOfEmblems[] = $arr_row[0];
        }

        return $arrayUniqueStringOfEmblems;
    }

    private function readDiapasonUniqueStringsByTags(int $amount, int $offset, array $tags): array {
        $tagsFormatForSQL = "''";
        foreach ($tags as $tag) {
            $tagsFormatForSQL .= ",'$tag'";
        }

        $query = "SELECT `uniqString` FROM ".self::TABLE_NAME." WHERE `tag` IN ($tagsFormatForSQL) GROUP BY `uniqString` LIMIT $amount OFFSET $offset";
        $resultQuery = mysqli_query($this->dbConnection, $query);

        $arrayUniqueStringOfEmblems = array();
        for ($i = 0; $i < $resultQuery->num_rows; $i++){
            $arr_row = $resultQuery->fetch_row();
            $arrayUniqueStringOfEmblems[] = $arr_row[0];
        }

        return $arrayUniqueStringOfEmblems;
    }

    private function readTagsEmblemByUniqueString(string $uniqueString): array {
        $query = "SELECT tag FROM ".self::TABLE_NAME." WHERE uniqString LIKE '$uniqueString'";
        $resultQuery = mysqli_query($this->dbConnection, $query);

        $arrayTagsByUniqueString = array();
        for ($i = 0; $i < $resultQuery->num_rows; $i++){
            $arr_row = $resultQuery->fetch_row();
            $arrayTagsByUniqueString[] = $arr_row[0];
        }

        return $arrayTagsByUniqueString;
    }

    private function checkTable(){
        $query = "CREATE TABLE Emblems_LoL
                    (
                      id        INT UNSIGNED AUTO_INCREMENT
                        PRIMARY KEY,
                      uniqString VARCHAR(255) NOT NULL,
                      tag       VARCHAR(30)  NOT NULL
                    )";
        mysqli_query($this->dbConnection, $query);
    }

    /*
     * ------------------------------------
     *  << Methods || Getters & Setters >>
     * ------------------------------------
     * */

    /**
     * @param \mysqli $dbConnection
     */
    public function setDbConnection(\mysqli $dbConnection)
    {
        $this->dbConnection = $dbConnection;
        $this->checkTable();
    }
}