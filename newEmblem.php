<?php
?>
<style>
    .form-adding{
        text-align: center;
        margin-left: 30%;
        margin-top: 3%;
        width: 35%;
    }
    .preview{
        margin-top: 3%;
        margin-left: 31%;
    }
    .attention{
        font-size: 18px;
        color: red;
    }
</style>
<script src="scripts/functions.js"></script>

<div><button onclick="window.history.back()">Back</button></div>

<div class="form-adding">
    <h3>You can add new Emblem or new tag for Emblem</h3>
    <form onsubmit="sendNewEmblem(this); return false;">
        Enter short URL of Emblem <br>for example /img/herald/asdfasdf_.st.jpg
        <input type="text" name="url" size="50" onkeyup="loadImg(this)" ><br>
        <img class="preview"
             id="img_loaded"
             src=""
             alt="Preview image"
             style="display: none;"
             onload="previewImg.statusLoading = true;"><br>
        Enter tags of Emblem through the space <input type="text" name="tags" size="30"><br>
        <input type="submit" style="cursor: pointer;">
    </form>
</div>