<?php
class VersionsData{
    private static $VERSION_APP_0_3_1 = "0.3.1";
    private static $VERSION_APP_0_3 = "0.3";

    public static function getLastVersionNumber(){
        return self::$VERSION_APP_0_3_1;
    }

    public static function getLastVersionFullText(){
        return self::getVersion0_3_1();
    }

    public static function getLastFiveChanges(){
        $resultText = self::getVersion0_3_1();
        $resultText .= self::getVersion0_3();
        return $resultText;
    }

    public static function getVersion0_3_1(){
        return "<h2>version ".self::$VERSION_APP_0_3_1."</h2>
        <ul>
            <li>Not loaded images will be hide</li>
        </ul>";
    }

    public static function getVersion0_3(){
        return "<h2>version ".self::$VERSION_APP_0_3."</h2>
        <ul>
            <li>Mouse hover on emblem will show tags instead URL</li>
            <li>Double click on emblem will copy unique string instead short URL</li>
            <li>Added display of the number of emblems in the database</li>
            <li>Added changelog</li>
            <li>Fixed pagination feature</li>
            <li>Added feature scrolling database using keyboard keys (left-right)</li>
        </ul>";
    }
}
