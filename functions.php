<?php

/**
 * Connect to mysql database
 * @param $host
 * @param $user
 * @param $pass
 * @param $dbName
 * @return mysqli
 */
function openMySqlConnection($host, $user, $pass, $dbName){
    $connection = mysqli_connect($host, $user, $pass, $dbName);
    $connection->set_charset("utf8");
    return $connection;
}

function closeMySqlConnection($connection){
    return mysqli_close($connection);
}

/**
 * Log request of client
 * @param FileLogger $logger: interface for logging
 * @param $query: is request of client emblem
 */
function logClientQuery(FileLogger $logger, string $query){
    $fileName = "./logs/".date("Y-m-d")."_clientQuery.log";
    $query = date("H:i:s >> ")."$query\n";
    $logger->log($fileName, $query);
}

function logError(FileLogger $logger, $text){
    $fileName = "./logs/".date("Y-m-d")."_err.log";
    $text = date("H:i:s >> ")."$text\n";
    $logger->log($fileName, $text);
}

function logClientSaving(FileLogger $logger, $url, $tags){
    $fileName = "./logs/".date("Y-m-d")."_clientSaving.log";
    $url = date("H:i:s >> ")."$url [$tags]\n";
    $logger->log($fileName, $url);
}
