<?php

interface FileLogger
{
    public function log($fileName, $data);
}