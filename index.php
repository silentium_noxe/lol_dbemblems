<?php

include "functions.php";
include "EmblemDAO.php";
include "VersionsData.php";
?>
<html>
<head>
    <link rel='stylesheet' href='stylesheets/emblems.css'>
    <link rel="stylesheet" href="stylesheets/styles.css">
    <script src='scripts/functions.js'></script>
    <script src='scripts/ImageList.js'></script>
    <script src='scripts/initPage.js'></script>
</head>
<body>
<script type="application/javascript">
    alert("Double click on image for copy unique string. " +
        "Click at version for looking news");
</script>

<div id="mainBlock-changelog" hidden onclick="hideChangelog()">
    <div class="secondBlock-changelog">
        <?php
            echo VersionsData::getLastFiveChanges();
        ?>
    </div>
</div>

<header style="display: flex">
<div class="form-searching">
    <input id="searchByTags"
           type="text"
           name="tags"
           list="listTags"
           multiple
           placeholder="enter tags through the space after press enter"
           title="empty will show all emblems"
           size="40"
           style="position: relative; top: -15px;">
    <datalist id="listTags"
            style="position: relative; top: -15px; width: 100px; cursor: pointer;"
            title="choose available tag"
            onchange="searchSelectedTag(this);">

<?php
$mysqli_connection = openMySqlConnection("localhost", "root", "root", "sn_LoL");

$emblemDAO = new EmblemDAO();
$emblemDAO->setDbConnection($mysqli_connection);

$tags = $emblemDAO->readAllTags();
array_splice($tags, 0, 1); //TODO: to fix
foreach ($tags as $tag){
    echo "<option value='$tag'>";
}

echo "</datalist>
<label for='searchByTags'><button id='button-search' onclick='imageList.pressButtonLoadByTags()'></button></label>
</div>
<div class='amountEmblems'>
<p id='p-amountEmblems'><span id='numberEmblems'>
".$emblemDAO->getAmountEmblems()."</span> Emblems already in database   
</p>
</div>
<div class='div-newEmblem'><button onclick='window.location = \"newEmblem.php\"'>Add new Emblem</div>
</header>

<input id='clipboard-input' hidden type='text'>
";
echo "<div id='emblems' style='display: flex'>";
echo "<button id='loadPreviousEmblems' 
        class='paginationButton paginationButtonLeft'
        onclick='imageList.loadPreviousImages()' 
        title='previous' 
        hidden></button>";
echo "<div id='listEmblems'></div>";
echo "<button id='loadNextEmblems' 
        class='paginationButton paginationButtonRight'
        onclick='imageList.loadNextImages()' 
        title='next'></button>";
echo "</div>";
?>

<div class="footer">
    <p>by Silentium.Noxe</p>
    <button class="btn-showChangelog" onclick="showChangelog()">
        version <?php echo VersionsData::getLastVersionNumber()?></button>
</div>

<script src="scripts/searchLine.js"></script>
<script type="application/javascript">initPage();</script>
</body>
</html>