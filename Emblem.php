<?php

class Emblem{
    private $uniqueString;
    private $arrTags = array();

    public function __construct(string $uniqueString)
    {
        $this->uniqueString = $uniqueString;
    }

    /*
     * ------------
     *  Methods >>
     * ------------
     * */

    public function addTag(string $tag){
        if (!$this->containTag($tag)) {
            $this->arrTags[] = $tag;
        }
    }

    public function removeTag(string $tag){
        $i = 0;
        foreach ($this->arrTags as $t){
            if ($t == $tag){
                array_splice($this->arrTags, $i, 1);
            }
            $i++;
        }
    }

    public function containTag(string $tag): bool {
        foreach ($this->arrTags as $t){
            if ($t == $tag){
                return true;
            }
        }
        return false;
    }

    public function containTags(array $tags): bool {
        $counter = 0;
        foreach ($tags as $tag1){
            foreach ($this->arrTags as $tag2){
                if ($tag1 == $tag2){
                    $counter++;
                }
            }
        }
        return $counter == count($tags);
    }

    public function compare(Emblem $emblem){
        if ($this === $emblem){
            return true;
        }

        if ($emblem->getClassName() != Emblem::class){
            return false;
        }

        //TODO: must be change
        if ($this->getUniqueString() == $emblem->getUniqueString()){
            $counter = 0;
            foreach ($this->getTags() as $tag1) {
                foreach ($emblem->getTags() as $tag2){
                    if ($tag1 == $tag2){
                        $counter++;
                    }
                }
            }
            if (count($this->getTags()) == $counter){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function getClassName(){
        return static::class;
    }

    /*
     * ----------------------------------
     *  << Methods || Getters & Setters >>
     * ----------------------------------
     * */

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->arrTags;
    }

    public function getTagsAsString(): string {
        return implode(" ", $this->arrTags);
    }

    public function getUniqueString(){
        return $this->uniqueString;
    }

    /**
     * Full link: https://landsoflords.com/img/_uniqueString_.st.jpg
     * @return string
     */
    public function getUrl(): string
    {
        return "https://www.landsoflords.com/img/herald/".$this->uniqueString.".st.jpg";
    }

    /*
     * -------------------------------------
     *  << Getters & Setters || Override >>
     * -------------------------------------
     * */

    public function __toString()
    {
        $out = "Emblem.class[uniqueString:'".$this->getUniqueString()."'; tags:[";
        $tags = $this->getTags();
        for ($i = 0; $i < count($tags); $i++){
            $out .= $tags[$i].";";
        }
        return $out."]]\r";
    }
}