var dataOfEmblem = {
    url: "",
    desc: ""
};

function setEventOnloader() {
    document.querySelector("#blzimg").onload = function (ev) {
        // dataOfEmblem.url = document.querySelector("#blzimg").src;
        // dataOfEmblem.desc = document.querySelector("#blzdesc").innerText;
        console.log(document.querySelector("#blzimg").src);
        console.log(document.querySelector("#blzdesc").innerText);
    }
}

function start(mode) {
    setEventOnloader();

    var xhr = new XMLHttpRequest();
    // xhr.setRequestHeader("Access-Control-Allow-Origin", "https://foo.app.moxio.com");
    xhr.open("GET", "https://emblems-lol.000webhostapp.com", true);
    xhr.onerror = function (ev) {
        if (mode === "OLD"){
            return;
        }
        reloadScript();
    };
    xhr.send();
}

function reloadScript() {
    document.querySelector(".blzbtn").click();
    start("OLD");
    // var urlOfEmblem = document.querySelector("#blzimg").src;
    // var descriptionOfEmblem = document.querySelector("#blzdesc").innerText;
}