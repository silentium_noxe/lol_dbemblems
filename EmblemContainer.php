<?php

include_once "Emblem.php";

class EmblemContainer
{
    private $arrEmblems;

    public function __construct()
    {
        $this->arrEmblems = array();
    }

    /*
     * ------------
     *  Methods >>
     * ------------
     * */

    public function addEmblem(Emblem $emblem){
        $isNewEmblem = true;
        foreach ($this->arrEmblems as $em){
            if (($em->getUniqueString() == $emblem->getUniqueString()) ? true:false){
                foreach ($emblem->getTags() as $tag){
                    if(!$em->containTag($tag)){
                        $isNewEmblem = false;
                        $em->addTag($tag);
                    }
                }
            }
        }
        if ($isNewEmblem) {
            $this->arrEmblems[] = $emblem;
        }

    }

    public function addEmblemFromMapData($map){
        foreach ($map as $key => $value){
            $emblem = new Emblem($key);
            foreach ($map[$key] as $item){
                $emblem->addTag($item);
            }
            $this->addEmblem($emblem);
        }
    }

    public function removeEmblemByLink(string $link){
        for ($i = 0; $i < count($this->arrEmblems); $i++){
            if ($this->arrEmblems[$i]->getUniqueString() == $link){
                array_splice($this->arrEmblems, $i, 1);
            }
        }
    }

    public function getArrayEmblems():array {
        return $this->arrEmblems;
    }

    public function getArrayEmblemsByTags(array $tags):array {
        $res = array();
        foreach ($this->arrEmblems as $emblem){
            if ($emblem->containTags($tags)){
                $res[] = $emblem;
            }
        }
        return $res;
    }
}