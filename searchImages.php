<?php

include "functions.php";
include "EmblemDAO.php";
include "CustomFileLogger.php";

$offsetEmblemsDB = $_POST["offset"];
$tagsString = $_POST["tags"];

$mysqli_connection = openMySqlConnection("localhost", "root", "root", "sn_LoL");

$emblemDAO = new EmblemDAO();
$emblemDAO->setDbConnection($mysqli_connection);
$container=new EmblemContainer();

if ($tagsString != "none"){
    if ($tagsString != null || $tagsString != " " || $tagsString != "") {
        $logger = new CustomFileLogger();
        logClientQuery($logger, $tagsString);

        $tagsString = strtolower($tagsString);
        $tagsString = str_replace(", ", " ", $tagsString);
        $tagsString = str_replace(",", " ", $tagsString);

        $tagsArray = explode(" ", $tagsString);
        $container = $emblemDAO->readDiapasonEmblemsWithTags(20, $offsetEmblemsDB, $tagsArray, $container);
    }
}else{
    $container = $emblemDAO->readDiapasonEmblemsWithoutTag(20, $offsetEmblemsDB, $container);
}

$emblemsForReturn = $container->getArrayEmblems();
if (count($emblemsForReturn)>0) {
    foreach ($emblemsForReturn as $item) {
        //FIXME: tags is null
        echo "<img title='" . $item->getTagsAsString() . "'
        src='" . $item->getUrl() . "'
        alt='".$item->getUniqueString()."'
        class='img-emblem'
        ondblclick='toClickBoard(this)'
        onerror='hideNotLoadedImage(this)'>";
    }
}else{
    echo "<h3>Nothing found</h3>";
}